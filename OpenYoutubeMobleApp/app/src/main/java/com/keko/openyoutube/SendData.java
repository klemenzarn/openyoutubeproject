package com.keko.openyoutube;

/**
 * Created by klemen on 5. 07. 2016.
 */
public class SendData {
    private String type;
    private String url;

    public SendData(String type, String url) {
        this.type = type;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
