package com.keko.openyoutube;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by klemen on 29. 06. 2016.
 */
public class OpenYoutubeService extends Service {

    public static WebSocketClient mWebSocketClient = null;
    private static final String MY_PREFS_NAME = "myPrefsIP";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String url = "ws://192.168.1.148:4080";
        if (intent != null) {
            if (intent.hasExtra("url")) {
                url = intent.getStringExtra("url");
            }
        } else{
            url = GetIP();
        }

        connectToServer(url);

        return super.onStartCommand(intent, flags, startId);
    }

    private String GetIP() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("url", "");
        return restoredText;
    }

    private void connectToServer(String url) {
        URI uri;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                Log.i("Websocket", "onMessage " + message);
                Intent startYoutubeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(message));
                startYoutubeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startYoutubeIntent);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();

    }
}
