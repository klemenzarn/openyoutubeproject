package com.keko.openyoutube;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    private static final String MY_PREFS_NAME = "myPrefsIP";
    private Button playButton;
    private EditText ipEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                sendYoutubeVideo(intent); // Handle text being sent
            }
        }


    }

    private void sendYoutubeVideo(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            Toast.makeText(MainActivity.this, sharedText, Toast.LENGTH_SHORT).show();
            SendData dataToSend = new SendData("fromApp", sharedText);
            String jsonData = new Gson().toJson(dataToSend);
            OpenYoutubeService.mWebSocketClient.send(jsonData); //TODO 
        }
    }

    private void starListenerService(String IP) {
        OpenYoutubeService openYoutubeService = new OpenYoutubeService();
        Intent serviceIntent = new Intent(this, openYoutubeService.getClass());
        if (IP.length() > 0)
            serviceIntent.putExtra("url", IP);
        startService(serviceIntent);
    }

    private void SaveIP(String IP) {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("url", IP);
        editor.commit();
    }

    private String GetIP() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString("url", "");
        return restoredText;
    }

    private void initComponents() {
        ipEditText = (EditText) findViewById(R.id.ipEditText);
        ipEditText.setText(GetIP());

        playButton = (Button) findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String IP = ipEditText.getText().toString();
                SaveIP(IP);
                starListenerService(IP);
            }
        });
    }
}
