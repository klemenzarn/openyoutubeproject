# OpenYoutubeProject

Odpiranje youtube posnetka (pošiljanje podatkov iz brskalnika v mobitel)

![alt tag](https://raw.githubusercontent.com/klemenkeko/OpenYoutubeProject/master/BrowserToMobile%20razlaga.png)

Odpiranje youtube posnetka (pošiljanje podatkov iz mobitel v brskalnik)

![alt tag](https://raw.githubusercontent.com/klemenkeko/OpenYoutubeProject/master/MobileToBrowser%20razlaga.png)
