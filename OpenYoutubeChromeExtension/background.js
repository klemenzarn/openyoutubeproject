// chrome.browserAction.onClicked.addListener(function(tab) { 
//     var tabUrl = tab.url;
//     sendYoutubeLink(tabUrl);    
// });

let puplDomApi = new PuplDomApi();

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    if (request == 'play') {
        playSelectedTab();
    } else if (request == 'stop') {
        stopPlaying();
    }
});

function playSelectedTab() {
    chrome.tabs.getSelected(null, tab => {
        var tabUrl = tab.url;

        sendYoutubeLink(tabUrl);
    });
}

function sendYoutubeLink(link){
    puplDomApi.play(link);
}

function stopPlaying(){
    puplDomApi.stop();
}