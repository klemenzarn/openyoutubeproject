// popup.js
window.onload = function () {
    document.getElementById("play").addEventListener("click", function () {
        chrome.runtime.sendMessage('play');
    });

    document.getElementById("stop").addEventListener("click", function () {
        chrome.runtime.sendMessage('stop');
    });
}

