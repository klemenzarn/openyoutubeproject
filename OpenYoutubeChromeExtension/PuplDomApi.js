class PuplDomApi {

    constructor() {
        this.apiUrl = 'http://pupldom/';
    }

    play(link) {
        if (link.indexOf('youtube.com') > -1) {
            link = encodeURIComponent(link);
            this.createRequest(`api/musicplayer/play?link=${link}`);         
        }
    }

    stop() {
        this.createRequest('api/musicplayer/stop');
    }

    createRequest(apiCallUrl, callback) {
        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            if (request.readyState === 4) {

                if (request.status === 200) {
                    //console.log('200')
                } else {
                    //console.log('An error occurred during your request: ' + request.status + ' ' + request.statusText);
                }
            }
        }

        request.open('GET', this.apiUrl + apiCallUrl);
        request.send();
    }
}