var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var opn = require('opn');
var routes = require('./routes/index');
var users = require('./routes/users');
//var gpio = require('rpi-gpio');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

var server = require('http').createServer(),
    url = require('url'),
    WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({ server: server }),
    port = 4081;

wss.on('connection', function connection(ws) {
    var location = url.parse(ws.upgradeReq.url, true);

    ws.on('message', function incoming(message) {

        var messageObject = JSON.parse(message);
        if (messageObject.type == 'sendUrl') {
            wss.broadcast(messageObject.url);
        } else if (messageObject.type == 'fromApp') {
            opn(messageObject.url);
            //todo namesto odpiranja preko opn poslati zahtevo na chrome extension
        }
        console.log('received: %s', messageObject.type);

    });

    console.log('connected client');
    //openLed(); //za raspberry PI led blinkanje
});

//raspbery pi pin nastavitve
var pinLed = 3;
var pinLed2 = 7;
var ledShown = false;
var ledShown2 = true;

function openLed() {
    gpio.write(pinLed, !ledShown, function(err) {
        if (err) throw err;
        console.log('Written to pin');
        ledShown = !ledShown;
    });

    /*gpio.write(pinLed2, !ledShown2, function(err) {
        if (err) throw err;
        console.log('Written to pin 2');
        ledShown2 = !ledShown2;
    });*/
}

process.on('SIGINT', function() {
    closeGPIO(); //zapremo vse porte
})

function closeGPIO() {
    console.log('closing pins');
    gpio.destroy(function() {
        console.log('Closed pins, now exit');
        process.exit();
    });
}

wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
        client.send(data);
    });
};

server.on('request', app);
server.listen(port, function() {
    console.log('Listening on ' + server.address().port)
    /*gpio.setup(pinLed, gpio.DIR_OUT, function() {
        gpio.setup(pinLed2, gpio.DIR_OUT, function() {
            console.log('set to output pin2');
            startBlinking();
        });
    });*/
});

function startBlinking() {
    setInterval(openLed, 1000);
}

module.exports = app;